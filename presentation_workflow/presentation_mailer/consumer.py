import json
import pika
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

parameters = pika.ConnectionParameters("rabbitmq")
connection = pika.BlockingConnection(parameters)
channel = connection.channel()


def process_approval(ch, method, properties, body):
    data = json.loads(body)
    name = data["presenter_name"]
    title = data["title"]
    email = data["presenter_email"]
    send_mail(
        "Your presentation has been accepted!",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted!",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


channel.queue_declare(queue="presentation_approvals")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=process_approval,
    auto_ack=True,
)


def process_rejection(ch, method, properties, body):
    data = json.loads(body)
    name = data["presenter_name"]
    title = data["title"]
    email = data["presenter_email"]
    send_mail(
        "Your presentation has been rejected. :(",
        f"{name}, unfortunately your presentation {title} has been rejected!",
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=process_rejection,
    auto_ack=True,
)


channel.start_consuming()
