from common.json import ModelEncoder

from .models import Attendee, AccountVO, ConferenceVO


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = [
        "name",
        "import_href",
    ]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
    ]

    def get_extra_data(self, o):
        return {"href": o.get_api_url()}


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["name", "email", "company_name", "created", "conference"]

    encoders = {"conference": ConferenceVODetailEncoder()}

    def get_extra_data(self, o):
        count = AccountVO.objects.filter(email=o.email).count()
        return {
            "has_account": count > 0,
        }
