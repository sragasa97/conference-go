from common.json import ModelEncoder
from events.encoders import ConferenceListEncoder

from .models import Presentation


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
    ]

    def get_extra_data(self, o):
        return {"status": o.get_status(), "href": o.get_api_url()}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {"conference": ConferenceListEncoder()}

    def get_extra_data(self, o):
        return {"status": o.get_status()}
