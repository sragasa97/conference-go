import requests
from .keys import PEXELS_KEY, OPEN_WEATHER_KEY


def get_weather(city, state):
    def get_coordinates():
        params = {
            "q": f"{city},{state},US",
            "appid": OPEN_WEATHER_KEY,
            "limit": 3,
        }

        url = "http://api.openweathermap.org/geo/1.0/direct"
        api_response = requests.get(url, params=params)

        return {
            "lat": api_response.json()[0]["lat"],
            "lon": api_response.json()[0]["lon"],
        }

    params = {
        "lat": get_coordinates()["lat"],
        "lon": get_coordinates()["lon"],
        "appid": OPEN_WEATHER_KEY,
        "units": "imperial",
    }

    url = "https://api.openweathermap.org/data/2.5/weather"
    api_response = (requests.get(url, params=params)).json()

    try:
        return {
            "temp": api_response["main"]["temp"],
            "description": api_response["weather"][0]["description"],
        }
    except KeyError or IndexError:
        return None


def get_photo(city, state):
    params = {
        "query": f"{city} {state}",
        "per_page": 3,
    }

    headers = {"Authorization": PEXELS_KEY}
    url = "https://api.pexels.com/v1/search"

    api_response = requests.get(url, params=params, headers=headers)

    try:
        return api_response.json()["photos"][0]["url"]
    except KeyError or IndexError:
        return None
